import {createAction, createReducer} from "redux-act";

export const setEvents = createAction('Set Event');
export const addEvent = createAction('Add Event');
export const editEvent = createAction('Edit Event');
export const deleteEvent = createAction('Delete Event');


const Events = createReducer({
    [setEvents] : (state,events) => events,
    [addEvent]: (state,event) => ([...state,event]),
    [deleteEvent]: (state,id) => state.filter(event => event._id !== id),
    [editEvent]: (state, payload) => state.map(event => event._id === payload.id ? {
        ...event,
        message: payload.message
    } : event)
});


export default Events;