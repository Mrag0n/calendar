import {
    editEvent as editEventPost,
    events as addEventPost,
    getEvents as getEventsGet,
    removeEvent as removeEventPost
} from "../api/events";
import {addEvent as addEventToStore, deleteEvent, editEvent, setEvents} from '../reducers/events'


export const addEvent = (data) => (dispatch) => {
    return addEventPost(data)
        .then((event) => {
            return dispatch(addEventToStore(event))
        });
};

export const removeEvent = (id) => (dispatch) => {
    return removeEventPost({id})
        .then(() => {
            return dispatch(deleteEvent(id));
    })
};

export const updateEvent = (id,message) => (dispatch) => {
    return editEventPost({id,message})
        .then(() => {
            return dispatch(editEvent({id,message}))
    });
};

export const getEvents = () => (dispatch) => {
    return getEventsGet()
        .then((events) => {
            return dispatch(setEvents(events));
        })
};

