import {post,get} from './index'

export const events = (data) => {
    return post('/addEvent', data)
};

export const removeEvent = (data) => {
    return post('/removeEvent', data)
};

export const editEvent = (data) => {
    return post('/editEvent', data)
};

export const getEvents = () => {
    return get('/getEvents')
};

