const mongoose = require('mongoose');
const crypto = require('crypto');

let dateSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: String,
    category: String,
    message: String
});

const Date = mongoose.model('Date', dateSchema);

module.exports = Date;